//
//  core.swift
//  Gift
//
//  Created by Bryan Heinz on 8/22/17.
//  Copyright © 2017 CookieBytes. All rights reserved.
//

import Foundation

struct Core {
    var killDL = false
    var captchaTriggered = false
    var badGifURL: [String] = []
    
    mutating func getHtml(giphyUrl: String, folderUrl: URL) {
        guard let myURL = URL(string: giphyUrl) else {
            print("Error: \(giphyUrl) doesn't seem to be a valid URL")
            badGifURL.append(giphyUrl)
            return
        }
        
        do {
            let myHTMLString = try String(contentsOf: myURL, encoding: .ascii)
            
            let gifUrl = returnGifUrl(htmlString: myHTMLString)
            
            if gifUrl.count < 2 {
                print("Error, url Gif URL returned.\n\(giphyUrl)")
                badGifURL.append(giphyUrl)
                if killDL {
                    return
                }
                return
            }
            
            guard let filename = gifUrl["name"] else {
                print("Error getting filename: \(gifUrl)")
                badGifURL.append(giphyUrl)
                return
            }
            
            guard let dlGifURL = gifUrl["url"] else {
                print("Error getting downloadable gif URL: \(gifUrl)")
                badGifURL.append(giphyUrl)
                return
            }
            
            let fileUrlToString = folderUrl.path
            let file = "\(fileUrlToString)/\(filename).gif"
            let fileUrl = URL(fileURLWithPath: file)
            
            
            downloader(url: URL(string: dlGifURL)!, to: fileUrl, completion: {
                return
            })
        } catch let error {
            print("Error: \(error)")
        }
    }
    
    mutating func returnGifUrl(htmlString: String) -> [String: String] {
        var gifDict = [String: String]()
        let htmlList = htmlString.components(separatedBy: .newlines)
        for line in htmlList {
            if line.range(of: "https://giphy.com/services/oembed?") != nil {
                let urlBlob = line.components(separatedBy: "=")
                let gifUrl = urlBlob[4].components(separatedBy: "\"")
                gifDict["url"] = gifUrl[0]
            }
            if line.lowercased().range(of: "canonical") != nil {
                let nameBlob = line.components(separatedBy: "gifs/")
                let nameBlobLast = nameBlob.last
                let name = nameBlobLast!.components(separatedBy: "\"")
                gifDict["name"] = name[0]
            }
            if line.lowercased().range(of: "captcha") != nil {
                print("Triggered Captcha, halting downloads.")
                killDL = true
                captchaTriggered = true
            }
        }
        return(gifDict)
    }
    
    func downloader(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url: url)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
//                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
//                    print("Success: \(statusCode)")
//                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion()
                } catch (let writeError) {
                    print("error writing file \(localUrl) : \(writeError)")
                }
                
            } else {
                print("Failure: %@", error ?? "download error.");
            }
        }
        task.resume()
    }
}
