//
//  ViewController.swift
//  Gift
//
//  Created by Bryan Heinz on 8/22/17.
//  Copyright © 2017 CookieBytes. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    var core = Core()
    
    @IBOutlet weak var urlBox: NSScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if let urlText = urlBox.documentView as? NSTextView {
//            let urlBlob = urlText.textStorage!.string
            guard let urlBlob = urlText.textStorage?.string else {
                print("Error getting URLs.")
                return
            }
            
            let urlArray = urlBlob.components(separatedBy: .newlines)
            
            let openPanel = NSOpenPanel()
            openPanel.allowsMultipleSelection = false
            openPanel.canChooseDirectories = true
            openPanel.canChooseFiles = false
            let okButton = openPanel.runModal()
            if okButton == NSApplication.ModalResponse.OK {
                var outText = "saved to \(openPanel.url!)"
                for url in urlArray {
                    core.getHtml(giphyUrl: url, folderUrl: openPanel.url!)
                    if core.killDL {
                        print("Downloads halted.")
                        if core.captchaTriggered {
                            captchaAlert()
                        }
                        break
                    }
                }
                if core.killDL {
                    outText = "Downloads halted.\nAny saved gifs can be found at\n\(openPanel.url!)."
                }
                if core.badGifURL.count > 0 {
                    let badUrlString = core.badGifURL.joined(separator: "\n\t")
                    outText = "\(outText)\nThe following URLs couldn't be downloaded:\n\t\(badUrlString)"
                }
                urlText.textStorage?.mutableString.setString(outText)
            }
        }
    }
    
    func captchaAlert() {
        let alert = NSAlert()
        alert.messageText = "Giphy Captcha Triggered"
        alert.informativeText = "Gift has triggered Giphy's captcha and cannot continue downloading. This is typically temporary and is triggered by attempting to download too many gifs at once."
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        alert.beginSheetModal(for: self.view.window!, completionHandler: nil)
    }

}
